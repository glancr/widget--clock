# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'clock/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name = 'clock'
  s.version = Clock::VERSION
  s.authors = ['Tobias Grasse']
  s.email = ['tg@glancr.de']
  s.homepage = 'https://glancr.de/modules/clock'
  s.summary = 'Widget for mirr.OS widget that shows the current time.'
  s.description = 'Shows the current time.'
  s.license = 'MIT'
  s.metadata = {
    'json' => {
      type: 'widgets',
      title: {
        enGb: 'Clock',
        deDe: 'Uhr',
        frFr: 'l\'horloge',
        esEs: 'Reloj',
        plPl: 'Zegar',
        koKr: '시계'
      },
      description: {
        enGb: s.description,
        deDe: 'Zeigt die aktuelle Uhrzeit.',
        frFr: 'Affiche l\'heure actuelle.',
        esEs: 'Muestra la hora actual.',
        plPl: 'Pokazuje aktualny czas.',
        koKr: '현재 시간을 표시합니다.'
      },
      sizes: [
        {
          w: 3,
          h: 1
        },
        {
          w: 3,
          h: 2
        },
        {
          w: 12,
          h: 6
        },
        {
          w: 21,
          h: 6
        },
        {
          w: 21,
          h: 12
        }
      ],
      languages: %i[enGb deDe frFr esEs koKr],
      group: nil
    }.to_json
  }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails', '~> 5.2'
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'rubocop-rails'
end
